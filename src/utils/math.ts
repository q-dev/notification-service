import BigNumber from 'bignumber.js';

export function transformPercentage (value: string | number) {
  return new BigNumber(value)
    .dividedBy('10000000000000000000000000')
    .toString();
}

export function percentage (val: string | number, total: string | number) {
  if (Number(total) === 0) return '0';

  return new BigNumber(val)
    .dividedBy(total)
    .multipliedBy(100)
    .toString();
}
