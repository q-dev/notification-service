import { ProposalType } from '@/typings/proposals';
import { I18nContext } from '@/typings/telegraf';

import { captureError } from '@/services/error-handler';

import { getProposalsKeyboard } from '@/helpers/keyboards';
import { createProposalsMessage, replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { getProposalsByType } from '@/helpers/proposals';

export function handleProposals (ctx: I18nContext) {
  replyWithHTML(ctx, [ctx.i18n.t('PROPOSALS_TITLE'), ctx.i18n.t('SELECT_PROPOSAL_TYPE')].join('\n\n'), {
    reply_markup: { inline_keyboard: getProposalsKeyboard(ctx) },
  });
}

export async function handleQProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'q');
}

export async function handleRootProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'rootNode');
}

export async function handleSlashingProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'slashing');
}

export async function handleExpertProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'expert');
}

export async function handleUpdateProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'contractUpdate');
}

export async function handleAllProposals (ctx: I18nContext) {
  sendProposalMessages(ctx, 'all');
}

async function sendProposalMessages (ctx: I18nContext, type: ProposalType) {
  try {
    const proposals = await getProposalsByType(type);
    const message = await createProposalsMessage(ctx, proposals, type);
    replyWithMenuLink(ctx, message);
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
