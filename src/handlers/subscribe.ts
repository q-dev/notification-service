import { createSubscription, ISubscription, readSubscription, updateSubscription } from '@/models/Subscription';

import { ProposalType } from '@/typings/proposals';
import { SubscribeEvent } from '@/typings/subscriprions';
import { I18nContext } from '@/typings/telegraf';

import { captureError } from '@/services/error-handler';

import { getSubscriptionsKeyboard } from '@/helpers/keyboards';
import { replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { translateProposalType } from '@/helpers/translations';

export async function handleSubscribe (ctx: I18nContext) {
  try {
    const sub = await readSubscription(ctx.chat.id);
    replyWithHTML(ctx, [ctx.i18n.t('SUBSCRIPTIONS_TITLE'), ctx.i18n.t('SELECT_SUBSCRIPTION_TYPE')].join('\n\n'), {
      reply_markup: { inline_keyboard: getSubscriptionsKeyboard(ctx, sub) },
    });
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleResetSubscribtions (ctx: I18nContext) {
  try {
    await updateSubscription(ctx.chat.id, {
      subQ: false,
      subRoot: false,
      subExpert: false,
      subSlashing: false,
      subUpdate: false,
      subDaily: false,
    });
    replyWithMenuLink(ctx, ctx.i18n.t('RESET_SUBSCRIPTIONS_SUCCESS'));
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleSubscribeQ (ctx: I18nContext) {
  subscribeOnProposalEvents(ctx, 'q');
}

export async function handleSubscribeRoot (ctx: I18nContext) {
  subscribeOnProposalEvents(ctx, 'rootNode');
}

export async function handleSubscribeSlashing (ctx: I18nContext) {
  subscribeOnProposalEvents(ctx, 'slashing');
}

export async function handleSubscribeExpert (ctx: I18nContext) {
  subscribeOnProposalEvents(ctx, 'expert');
}

export async function handleSubscribeUpdate (ctx: I18nContext) {
  subscribeOnProposalEvents(ctx, 'contractUpdate');
}

export async function handleSubscribeAll (ctx: I18nContext) {
  try {
    const newSubscription: ISubscription = {
      _id: ctx.chat.id,
      subQ: true,
      subRoot: true,
      subExpert: true,
      subSlashing: true,
      subUpdate: true,
      subDaily: true
    };

    const subscription = await readSubscription(ctx.chat.id);
    if (!subscription) {
      await createSubscription(newSubscription);
    }

    await updateSubscription(ctx.chat.id, newSubscription);
    replyWithMenuLink(ctx, ctx.i18n.t('SUBSCRIBED_SUCCESS', {
      title: ctx.i18n.t('ALL_PROPOSALS'),
    }));
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleDailySubscriptions (ctx: I18nContext) {
  try {
    const subscription = await readSubscription(ctx.chat.id);
    const newSubscription: Partial<ISubscription> = {
      _id: ctx.chat.id,
      subDaily: !subscription?.subDaily,
    };

    if (!subscription) {
      await createSubscription(newSubscription);
    }

    await updateSubscription(ctx.chat.id, newSubscription);

    const message = newSubscription.subDaily
      ? ctx.i18n.t('SUBSCRIBED_SUCCESS', { title: ctx.i18n.t('DAILY_REPORT') })
      : ctx.i18n.t('UNSUBSCRIBED_SUCCESS', { title: ctx.i18n.t('DAILY_REPORT') });
    replyWithMenuLink(ctx, message);
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

async function subscribeOnProposalEvents (ctx: I18nContext, type: Exclude<ProposalType, 'all'>) {
  const typeToEventsMap: Record<Exclude<ProposalType, 'all'>, SubscribeEvent> = {
    q: 'subQ',
    rootNode: 'subRoot',
    expert: 'subExpert',
    slashing: 'subSlashing',
    contractUpdate: 'subUpdate',
  };

  try {
    const subscription = await readSubscription(ctx.chat.id);
    const title = translateProposalType(type, ctx);
    const subEvent = typeToEventsMap[type];

    if (!subscription) {
      const newSub: Partial<ISubscription> = {
        _id: ctx.chat.id,
        [subEvent]: true,
      };
      await createSubscription(newSub);
      replyWithMenuLink(ctx, ctx.i18n.t('SUBSCRIBED_SUCCESS', { title }));
      return;
    }

    if (!subscription[subEvent]) {
      await updateSubscription(ctx.chat.id, { [subEvent]: true });
      replyWithMenuLink(ctx, ctx.i18n.t('SUBSCRIBED_SUCCESS', { title }));
      return;
    }

    await updateSubscription(ctx.chat.id, { [subEvent]: false });
    replyWithMenuLink(ctx, ctx.i18n.t('UNSUBSCRIBED_SUCCESS', { title }));
  } catch (error) {
    captureError(error, type);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
