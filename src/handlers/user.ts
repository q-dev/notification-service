import { Message, Update } from 'telegraf/typings/core/types/typegram';

import { deleteSubscription } from '@/models/Subscription';
import { addUserAddress, createUser, deleteUser, deleteUserAddress, readUser } from '@/models/User';

import { ADDRESSES_LIMIT } from '@/constants/adresses';
import { I18nContext } from '@/typings/telegraf';

import { captureError } from '@/services/error-handler';
import { web3 } from '@/services/web3';

import { getBackToMenuKeyboard } from '@/helpers/keyboards';
import { replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { trimAddress } from '@/utils/strings';

export async function handleClearAccount (ctx: I18nContext) {
  try {
    const user = await readUser(ctx.chat.id);
    if (user) {
      await deleteUser(ctx.chat.id);
      await deleteSubscription(ctx.chat.id);
    }

    replyWithMenuLink(ctx, ctx.i18n.t('ACCOUNT_DELETED'));
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleShowAddress (ctx: I18nContext) {
  try {
    const userData = await readUser(ctx.chat.id);
    const addresses = userData?.address ?? [];
    if (addresses.length === 0) {
      replyWithMenuLink(ctx, ctx.i18n.t('NO_ADRESSES'));
      return;
    }

    replyWithHTML(ctx, ctx.i18n.t('REMOVE_ADDRESS'), {
      reply_markup: {
        inline_keyboard: [
          ...addresses.map((address) => [{
            text: ctx.i18n.t('REMOVE_ADDRESS_ITEM', { address: trimAddress(address) }),
            callback_data: address
          }]),
          ...getBackToMenuKeyboard(ctx),
        ],
      },
    });
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleAddAddress (ctx: I18nContext) {
  try {
    const text = (ctx.message as Message.TextMessage).text.split(' ')[1] ?? '';
    const message = web3.utils.isAddress(text)
      ? await saveUserAddress(ctx, text)
      : ctx.i18n.t('INVALID_ADDRESS');
    replyWithMenuLink(ctx, message);
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleRemoveAddress (ctx: I18nContext) {
  try {
    const callbackQuery = (ctx as I18nContext<Update.CallbackQueryUpdate>).update.callback_query.data || '';
    if (web3.utils.isAddress(callbackQuery)) {
      await deleteUserAddress(ctx.chat.id, callbackQuery);
      replyWithMenuLink(ctx, ctx.i18n.t('ADDRESS_REMOVED'));
    }
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

async function saveUserAddress (ctx: I18nContext, address: string) {
  const user = await readUser(ctx.chat.id);
  if (!user) {
    const name = 'first_name' in ctx.chat ? ctx.chat.first_name : ctx.chat.title;
    await createUser({ _id: ctx.chat.id, name, address: [address] });
    return ctx.i18n.t('ADDRESS_ADDED', { count: 1 });
  }

  if (user.address.includes(address)) {
    return ctx.i18n.t('ADDRESS_EXISTS');
  }

  if (user.address.length < ADDRESSES_LIMIT) {
    await addUserAddress(ctx.chat.id, address);
    return ctx.i18n.t('ADDRESS_ADDED', { count: user.address.length + 1 });
  }

  return ctx.i18n.t('ADDRESS_LIMIT_REACHED', { limit: ADDRESSES_LIMIT });
}
