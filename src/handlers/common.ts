import { Chat } from 'telegraf/typings/core/types/typegram';

import { createUser, readUser, updateUserLanguage } from '@/models/User';

import { ADDRESSES_LIMIT } from '@/constants/adresses';
import { I18nContext, Language } from '@/typings/telegraf';

import { captureError } from '@/services/error-handler';
import { getI18nContext } from '@/services/i18n';

import { getAccountKeyboard, getLanguagesKeyboard, getMainMenuKeyboard } from '@/helpers/keyboards';
import { replyWithHTML, replyWithMenuLink } from '@/helpers/messages';

export async function handleStart (ctx: I18nContext) {
  const welcomeMsg = ctx.message?.chat.type === 'group'
    ? ctx.i18n.t('WELCOME_GROUP_MSG')
    : ctx.i18n.t('WELCOME_PRIVATE_MSG', {
      name: (ctx.chat as Chat.PrivateChat)?.first_name
    });

  replyWithHTML(ctx, [welcomeMsg, ctx.i18n.t('WELCOME_DESCRIPTION')].join('\n\n'), {
    reply_markup: { inline_keyboard: getMainMenuKeyboard(ctx) },
  });
}

export async function handleMenu (ctx: I18nContext) {
  replyWithHTML(
    ctx,
    [ctx.i18n.t('MENU_TITLE'), ctx.i18n.t('MENU_DESCRIPTION')].join('\n\n'),
    { reply_markup: { inline_keyboard: getMainMenuKeyboard(ctx) } }
  );
}

export async function handleAccount (ctx: I18nContext) {
  const user = await readUser(ctx.chat.id);

  replyWithHTML(ctx, [
    ctx.i18n.t('ACCOUNT_TITLE'),
    ctx.i18n.t('ACCOUNT_DESCRIPTION', { limit: ADDRESSES_LIMIT })
  ].join('\n\n'), {
    reply_markup: { inline_keyboard: getAccountKeyboard(ctx, user) },
  });
}

export async function handleHelp (ctx: I18nContext) {
  const message = [
    ctx.i18n.t('HELP_TITLE'),
    ctx.i18n.t('HELP_PROPOSALS_TOPIC'),
    ctx.i18n.t('HELP_SUBSCRIPTIONS_TOPIC'),
    ctx.i18n.t('HELP_ADDRESS_TOPIC', { limit: ADDRESSES_LIMIT }),
  ].join('\n\n');

  replyWithMenuLink(ctx, message);
}

export function handleLanguage (ctx: I18nContext) {
  replyWithHTML(ctx, ctx.i18n.t('LANGUAGE_DESCRIPTION'), {
    reply_markup: { inline_keyboard: getLanguagesKeyboard(ctx) },
  });
}

export async function handleChangeLanguage (ctx: I18nContext, lang: Language) {
  try {
    const user = await readUser(ctx.chat.id);
    if (!user) {
      const name = 'first_name' in ctx.chat ? ctx.chat.first_name : ctx.chat.title;
      await createUser({ _id: ctx.chat.id, name, address: [] });
    }

    await updateUserLanguage(ctx.chat.id, lang);
    const i18nContext = await getI18nContext(ctx.chat.id);
    ctx.i18n = i18nContext.i18n;
    replyWithMenuLink(ctx, ctx.i18n.t('LANGUAGE_CHANGED'));
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
