import mongoose from 'mongoose';

export interface ISubscription {
  _id: number;
  subQ: boolean;
  subRoot: boolean;
  subExpert: boolean;
  subSlashing: boolean;
  subUpdate: boolean;
  subDaily: boolean;
}

export const SubscriptionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  subQ: { type: Boolean, default: true },
  subRoot: { type: Boolean, default: true },
  subExpert: { type: Boolean, default: true },
  subSlashing: { type: Boolean, default: true },
  subUpdate: { type: Boolean, default: true },
  subDaily: { type: Boolean, default: false },
}, { collection: 'Subscription' });

const Subscription = mongoose.model<ISubscription>('Subscription', SubscriptionSchema);
export default Subscription;

export function createSubscription (subscription: Partial<ISubscription>) {
  const newSubscription = new Subscription(subscription);
  return newSubscription.save();
}

export function readSubscription (id: number) {
  return Subscription.findOne({ _id: id });
}

export function readAllSubscriptions () {
  return Subscription.find();
}

export function updateSubscription (id: number, subscription: Partial<ISubscription>) {
  return Subscription.updateOne({ _id: id }, { $set: subscription });
}

export function deleteSubscription (id: number) {
  return Subscription.deleteOne({ _id: id });
}
