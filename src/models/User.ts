import mongoose from 'mongoose';

import { Language } from '@/typings/telegraf';

export interface IUser {
  _id: number;
  name: string;
  language?: Language;
  address: string[];
}

export const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true },
  language: { type: String, default: 'en' },
  address: { type: Array<String>, required: true },
}, { collection: 'User' });

const User = mongoose.model<IUser>('User', UserSchema);
export default User;

export async function createUser (user: IUser) {
  const newUser = new User(user);
  await newUser.save();
  return newUser;
}

export async function readUser (id: number) {
  return User.findOne({ _id: id });
}

export async function deleteUser (id: number) {
  await User.deleteOne({ _id: id });
}

export async function addUserAddress (id: number, address: string) {
  await User.updateOne({ _id: id }, { $push: { address } });
}

export async function updateUserLanguage (id: number, language: Language) {
  await User.updateOne({ _id: id }, { language });
}

export async function deleteUserAddress (id: number, address: string) {
  await User.updateOne({ _id: id }, { $pull: { address } });
}
