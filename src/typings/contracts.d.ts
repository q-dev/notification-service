import { ContractRegistryInstance } from '@q-dev/q-js-sdk';

export type ProposalContractType =
  | 'addressVoting'
  | 'upgradeVoting'
  | 'constitutionVoting'
  | 'emergencyUpdateVoting'
  | 'generalUpdateVoting'
  | 'rootNodesMembershipVoting'
  | 'rootNodesSlashingVoting'
  | 'validatorsSlashingVoting'
  | 'epqfiMembershipVoting'
  | 'epdrMembershipVoting'
  | 'epqfiParametersVoting'
  | 'epdrParametersVoting'
  | 'eprsMembershipVoting'
  | 'eprsParametersVoting';

export type ContractType = ProposalContractType | 'rootNodes';
export type ContractValue<T extends ContractType> = ReturnType<ContractRegistryInstance[T]>;
