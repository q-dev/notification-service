export type SubscribeEvent = 'subQ' | 'subRoot' | 'subExpert' | 'subSlashing' | 'subUpdate';
