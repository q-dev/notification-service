import { Classification, ProposalStatus, SetKeyProposal, UpgradeProposal } from '@q-dev/q-js-sdk';
import { ConstitutionProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/constitution/ConstitutionVotingInstance';
import { EmergencyProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/EmergencyUpdateVotingInstance';
import { ExpertMembershipProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/experts/ExpertsMembershipVotingInstance';
import { ExpertParameterProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/experts/ExpertsParametersVotingInstance';
import { GeneralProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/GeneralUpdateVotingInstance';
import { RootsProposal } from '@q-dev/q-js-sdk/lib/contracts/governance/rootNodes/RootNodesMembershipVotingInstance';
import { SlashingProposal } from '@q-dev/q-js-sdk/lib/contracts/SlashingVotingHelperInstance';

import { ProposalContractType } from './contracts';

export type ProposalType = 'q' | 'rootNode' | 'expert' | 'slashing' | 'contractUpdate' | 'all';

export type ContractProposal =
  | ConstitutionProposal
  | EmergencyProposal
  | GeneralProposal
  | RootsProposal
  | SlashingProposal
  | ExpertMembershipProposal
  | ExpertParameterProposal
  | SetKeyProposal
  | UpgradeProposal;

export interface ProposalEvent {
  blockNumber: number;
  id: string;
  contract: ProposalContractType;
  status?: string;
}

export interface Proposal {
  id: string;
  type: ProposalContractType;
  status: ProposalStatus;
  blockNumber: number;

  votesFor: number;
  votesAgainst: number;

  voteEndTime: number;
  vetoEndTime: number;

  forPercentage: string;
  againstPercentage: string;

  currentQuorum: string;
  requiredQuorum: string;

  remark: string;
  candidate: string;
  replaceDest: string;
  classification: Classification;

  proxy: string;
  implementation: string;
  key: string;

  addressesVoted: string[];
}
