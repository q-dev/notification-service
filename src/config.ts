import 'dotenv/config';

export const config = {
  TELEGRAM_TOKEN: String(process.env.TELEGRAM_TOKEN || ''),
  DATABASE_URL: String(process.env.DATABASE_LINK || ''),
  WEBSOCKET_URL: String(process.env.WEBSOCKET_LINK || ''),
  DAPP_LINK: String(process.env.DAPP_LINK || ''),
  CONTRACT_REGISTRY_ADDRESS: String(process.env.CONTRACT_REGISTRY_ADDRESS || ''),
  SENTRY: String(process.env.SENTRY || ''),
  MONITORING_PORT: String(process.env.MONITORING_PORT || ''),
};
