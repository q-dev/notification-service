
import { ProposalContractType } from '@/typings/contracts';
import { Proposal } from '@/typings/proposals';
import { SubscribeEvent } from '@/typings/subscriprions';
import { I18nContext } from '@/typings/telegraf';

import { translateProposalType } from './translations';

export function getProposalSubscribeEvent (type: ProposalContractType): SubscribeEvent {
  switch (type) {
    case 'constitutionVoting':
    case 'emergencyUpdateVoting':
    case 'generalUpdateVoting':
      return 'subQ';
    case 'epqfiMembershipVoting':
    case 'epqfiParametersVoting':
    case 'epdrParametersVoting':
    case 'epdrMembershipVoting':
    case 'eprsParametersVoting':
    case 'eprsMembershipVoting':
      return 'subExpert';
    case 'rootNodesMembershipVoting':
      return 'subRoot';
    case 'rootNodesSlashingVoting':
    case 'validatorsSlashingVoting':
      return 'subSlashing';
    case 'upgradeVoting':
    case 'addressVoting':
      return 'subUpdate';
  }
}

export function getProposalsSummary (ctx: I18nContext, combinedProposals: Proposal[][]) {
  const [
    qProposals,
    rootProposals,
    expertProposals,
    slashingProposals,
    updateProposals,
  ] = combinedProposals;

  const proposalStats: { title: string; count: number }[] = [
    { title: translateProposalType('q', ctx), count: qProposals.length },
    { title: translateProposalType('expert', ctx), count: expertProposals.length },
    { title: translateProposalType('rootNode', ctx), count: rootProposals.length },
    { title: translateProposalType('slashing', ctx), count: slashingProposals.length },
    { title: translateProposalType('contractUpdate', ctx), count: updateProposals.length },
  ];

  return [
    `${ctx.i18n.t('STATUS_REPORT_TITLE')}\n`,
    ...proposalStats.map(({ title, count }) => ctx.i18n.t('STATUS_REPORT_ITEM', { title, count })),
  ].join('\n');
}
