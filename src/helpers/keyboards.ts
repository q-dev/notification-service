import { InlineKeyboardButton } from 'telegraf/typings/core/types/typegram';

import { ISubscription } from '@/models/Subscription';
import { IUser } from '@/models/User';

import { ADDRESSES_LIMIT } from '@/constants/adresses';
import { BotActions } from '@/constants/bot-actions';
import { I18nContext } from '@/typings/telegraf';

export function getMainMenuKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('PROPOSALS'), callback_data: BotActions.PROPOSALS },
      { text: ctx.i18n.t('SUBSCRIPTIONS'), callback_data: BotActions.SUBSCRIPTIONS },
    ],
    [
      { text: ctx.i18n.t('ACCOUNT'), callback_data: BotActions.ACCOUNT },
      { text: ctx.i18n.t('HELP'), callback_data: BotActions.HELP },
    ]
  ];
}

export function getProposalsKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('Q_PROPOSALS'), callback_data: BotActions.Q_PROPOSALS },
      { text: ctx.i18n.t('EXPERT_PROPOSALS'), callback_data: BotActions.EXPERT_PROPOSALS },
    ],
    [
      { text: ctx.i18n.t('ROOT_NODE_PROPOSALS'), callback_data: BotActions.ROOT_PROPOSALS },
      { text: ctx.i18n.t('SLASHING_PROPOSALS'), callback_data: BotActions.SLASHING_PROPOSALS },
    ],
    [{ text: ctx.i18n.t('CONTRACT_UPDATE_PROPOSALS'), callback_data: BotActions.UPDATES_PROPOSALS }],
    [{ text: ctx.i18n.t('ALL_PROPOSALS'), callback_data: BotActions.ALL_PROPOSALS }],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getSubscriptionsKeyboard (ctx: I18nContext, sub: ISubscription | null): InlineKeyboardButton[][] {
  const getStatusText = (isEnabled?: boolean) => {
    return isEnabled ? ctx.i18n.t('SUBSCRIPTION_ON') : ctx.i18n.t('SUBSCRIPTION_OFF');
  };

  return [
    [
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('Q_PROPOSALS'),
          status: getStatusText(sub?.subQ),
        }),
        callback_data: BotActions.SUBSCRIBE_Q
      },
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('EXPERT_PROPOSALS'),
          status: getStatusText(sub?.subExpert),
        }),
        callback_data: BotActions.SUBSCRIBE_EXPERT
      },
    ],
    [
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('ROOT_NODE_PROPOSALS'),
          status: getStatusText(sub?.subRoot),
        }),
        callback_data: BotActions.SUBSCRIBE_ROOT
      },
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('SLASHING_PROPOSALS'),
          status: getStatusText(sub?.subSlashing),
        }),
        callback_data: BotActions.SUBSCRIBE_SLASHING
      },
    ],
    [
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('CONTRACT_UPDATE_PROPOSALS'),
          status: getStatusText(sub?.subUpdate),
        }),
        callback_data: BotActions.SUBSCRIBE_UPDATE
      }
    ],
    [
      {
        text: ctx.i18n.t('SUBSCRIPTION_MENU_ITEM', {
          name: ctx.i18n.t('DAILY_REPORT'),
          status: getStatusText(sub?.subDaily),
        }),
        callback_data: BotActions.DAILY_SUBSCRIPTIONS
      }
    ],
    [
      { text: ctx.i18n.t('SUBSCRIBE_ALL'), callback_data: BotActions.SUBSCRIBE_ALL },
      { text: ctx.i18n.t('UNSUBSCRIBE_ALL'), callback_data: BotActions.RESET_SUBSCRIPTIONS },
    ],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getAccountKeyboard (ctx: I18nContext, user: IUser | null): InlineKeyboardButton[][] {
  return [
    [
      {
        text: ctx.i18n.t('ADDRESSES', { count: user?.address.length || 0, max: ADDRESSES_LIMIT }),
        callback_data: BotActions.ADDRESSES
      },
      { text: ctx.i18n.t('LANGUAGE'), callback_data: BotActions.LANGUAGE },
    ],
    [{ text: ctx.i18n.t('CLEAR_ACCOUNT_DATA'), callback_data: BotActions.CLEAR_ACCOUNT }],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getBackToMenuKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getLanguagesKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('ENGLISH'), callback_data: BotActions.ENGLISH },
      // TODO: Enable when German translations added
      // { text: ctx.i18n.t('GERMAN'), callback_data: BotActions.GERMAN },
      { text: ctx.i18n.t('UKRAINIAN'), callback_data: BotActions.UKRAINIAN },
    ],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}
