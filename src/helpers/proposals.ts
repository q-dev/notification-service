import { BaseProposal, ProposalStatus, UpgradeProposal, VotingStats } from '@q-dev/q-js-sdk';
import { flatten } from 'lodash';

import {
  expertContractTypes,
  expertParametersContractTypes,
  noVetoContractTypes,
  proposalContractTypes,
  qContractTypes,
  rootNodeContractTypes,
  slashingContractTypes,
  updateContractTypes
} from '@/constants/contracts';
import { ProposalContractType } from '@/typings/contracts';
import { ContractProposal, Proposal, ProposalEvent, ProposalType } from '@/typings/proposals';

import { getContractInstance, web3 } from '@/services/web3';

import { percentage, transformPercentage } from '@/utils/math';

const activeBlockNumbersMap: Partial<Record<ProposalContractType, number>> = {};

export async function getProposalsByType (type: ProposalType) {
  const contractTypesMap: Record<ProposalType, ProposalContractType[]> = {
    q: qContractTypes,
    rootNode: rootNodeContractTypes,
    expert: expertContractTypes,
    slashing: slashingContractTypes,
    contractUpdate: updateContractTypes,
    all: proposalContractTypes
  };

  const newProposals = await Promise.all(contractTypesMap[type].map(getContractProposals));
  return flatten(newProposals);
}

export async function getContractProposals (type: ProposalContractType) {
  const proposalEvents = await getProposalPastEvents(type);
  const blockNumber = await web3.eth.getBlockNumber();

  const proposals = await Promise.all(
    proposalEvents.map((event) => getProposal(event, type))
  );
  const activeProposals = proposals.filter((proposal) => {
    return [ProposalStatus.PENDING, ProposalStatus.ACCEPTED, ProposalStatus.PASSED]
      .includes(proposal.status);
  });

  activeBlockNumbersMap[type] = activeProposals.length > 0
    ? Math.min(...activeProposals.map(p => p.blockNumber))
    : blockNumber;

  return activeProposals;
}

async function getProposalPastEvents (type: ProposalContractType): Promise<ProposalEvent[]> {
  const contract = await getContractInstance(type);
  const pastEvents = await contract.instance.getPastEvents(
    'ProposalCreated',
    {
      fromBlock: activeBlockNumbersMap[type] || 0,
      toBlock: 'latest'
    }
  );

  return pastEvents.map((evt) => ({
    blockNumber: evt.blockNumber,
    id: evt.returnValues._id || evt.returnValues._proposalId,
    contract: type,
  }));
}

async function getProposal (event: ProposalEvent, type: ProposalContractType): Promise<Proposal> {
  const contract = await getContractInstance(type);
  const rootInstance = await getContractInstance('rootNodes');

  const proposal = await contract.getProposal(event.id) as ContractProposal;
  const status = await contract.getStatus(event.id);
  const stats = await contract.getProposalStats(event.id) as VotingStats;
  const rootNodesNumber = await rootInstance.getSize();

  const voteCount = 'voteCount' in contract.instance.methods
    ? await contract.instance.methods.voteCount(event.id).call()
    : '0';

  const base = type === 'emergencyUpdateVoting' || type === 'generalUpdateVoting' || !('base' in proposal)
    // TODO: Fix SDK
    ? proposal as unknown as BaseProposal
    : proposal.base;

  const hasNoVeto = noVetoContractTypes.includes(type);
  const hasMemberVotes = hasNoVeto || expertParametersContractTypes.includes(type);

  const votesFor = hasMemberVotes
    ? updateContractTypes.includes(type)
      ? Number(voteCount)
      : Number(base.counters.weightFor)
    : Number(web3.utils.fromWei(base.counters.weightFor));
  const votesAgainst = hasMemberVotes
    ? updateContractTypes.includes(type)
      ? Number(rootNodesNumber) - Number(voteCount)
      : Number(base.counters.weightAgainst)
    : Number(web3.utils.fromWei(base.counters.weightAgainst));
  const votesCount = votesFor + votesAgainst;

  return {
    id: event.id,
    type,
    status,
    blockNumber: event.blockNumber,

    votesFor,
    votesAgainst,
    voteEndTime: updateContractTypes.includes(type)
      ? Number((proposal as UpgradeProposal).votingExpiredTime) * 1000
      : 'votingEndTime' in base.params
        ? Number(base.params.votingEndTime) * 1000
        : 0,
    vetoEndTime: !updateContractTypes.includes(type) && 'vetoEndTime' in base.params
      ? Number(base.params.vetoEndTime) * 1000
      : 0,

    forPercentage: percentage(votesFor, votesCount),
    againstPercentage: percentage(votesAgainst, votesCount),

    currentQuorum: 'currentQuorum' in stats
      ? transformPercentage(stats.currentQuorum)
      : '0',
    requiredQuorum: 'requiredQuorum' in stats
      ? transformPercentage(stats.requiredQuorum)
      : '0',

    remark: base.remark,
    proxy: 'proxy' in proposal ? proposal.proxy : '',
    implementation: 'implementation' in proposal ? proposal.implementation : '',
    key: 'key' in proposal ? proposal.key : '',
    candidate: 'candidate' in proposal ? proposal.candidate : '',
    replaceDest: 'replaceDest' in proposal ? proposal.replaceDest : '',
    classification: 'classification' in proposal ? proposal.classification : '',

    addressesVoted: [],
  } as Proposal;
}

export async function getVotedAddresses (proposal: Proposal, userAddresses: string[] = []) {
  const instance = await getContractInstance(proposal.type);
  const addressesVoted = await Promise.all(userAddresses.map(address => {
    return 'hasUserVoted' in instance
      ? instance.hasUserVoted(proposal.id, address)
      : false;
  }));

  return userAddresses.filter((_, i) => addressesVoted[i]);
}
