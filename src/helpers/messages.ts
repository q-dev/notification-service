import { Context } from 'telegraf';
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types';

import { readUser } from '@/models/User';

import { config } from '@/config';
import { slashingContractTypes, updateContractTypes } from '@/constants/contracts';
import { PROPOSALS_TO_SHOW_LIMIT } from '@/constants/proposals';
import { Proposal, ProposalType } from '@/typings/proposals';
import { I18nContext } from '@/typings/telegraf';

import { bot } from '@/app';
import { captureError } from '@/services/error-handler';

import { formatDate, formatNumber, formatPercent } from '@/utils/formatters';

import { getBackToMenuKeyboard } from './keyboards';
import { getVotedAddresses } from './proposals';
import { getProposalTitle, translateProposalStatus, translateProposalType } from './translations';

export async function sendMessageToChat (
  chatId: number,
  message: string,
  extra: ExtraReplyMessage = {}
) {
  try {
    await bot.telegram.sendMessage(chatId, message, {
      ...extra,
      parse_mode: 'HTML',
      disable_web_page_preview: true
    });
  } catch (error) {
    captureError(error);
  }
}

export function replyWithHTML (
  ctx: Context,
  message: string,
  extra: ExtraReplyMessage = {}
) {
  return ctx.reply(message, {
    ...extra,
    disable_web_page_preview: true,
    parse_mode: 'HTML',
  });
}

export function replyWithMenuLink (ctx: I18nContext, message: string) {
  return replyWithHTML(ctx, message, {
    reply_markup: { inline_keyboard: getBackToMenuKeyboard(ctx) },
  });
}

export async function createProposalsMessage (
  ctx: I18nContext,
  activeProposals: Proposal[],
  type: ProposalType
) {
  const title = translateProposalType(type, ctx);
  if (!activeProposals.length) return ctx.i18n.t('NO_ACTIVE_PROPOSALS_MSG', { title });
  if (activeProposals.length > PROPOSALS_TO_SHOW_LIMIT) return ctx.i18n.t('TOO_MANY_PROPOSALS_MSG', { title });

  const user = await readUser(ctx.chat.id);

  const proposals = await Promise.all(
    activeProposals.map(async (proposal) => {
      const addressesVoted  = await getVotedAddresses(proposal, user?.address);
      return { ...proposal, addressesVoted };
    })
  );

  const proposalMessages = proposals
    .map((proposal) => createProposalMessage(ctx, proposal, user?.address));

  return `${ctx.i18n.t('ACTIVE_PROPOSALS', { title })}\n\n${proposalMessages.join('\n\n')}`;
}

export async function createNewProposalMessage (ctx: I18nContext, proposal: Proposal) {
  const user = await readUser(ctx.chat.id);
  const addressesVoted = await getVotedAddresses(proposal, user?.address);
  return `${ctx.i18n.t('NEW_PROPOSAL_CREATED')}\n\n${createProposalMessage(ctx, { ...proposal, addressesVoted }, user?.address, true)}`;
}

function createProposalMessage (
  ctx: I18nContext,
  proposal: Proposal,
  addresses: string[] = [],
  short = false
) {
  const votesFor = formatNumber(proposal.votesFor);
  const forPercentage = formatPercent(proposal.forPercentage);
  const votesAgainst = formatNumber(proposal.votesAgainst);
  const againstPercentage = formatPercent(proposal.againstPercentage);

  const votingEndTime = formatDate(proposal.voteEndTime, ctx.language);
  const vetoEndTime = formatDate(proposal.vetoEndTime, ctx.language);

  const currentQuorum = formatPercent(proposal.currentQuorum);
  const requiredQuorum = formatPercent(proposal.requiredQuorum);

  const vote = proposal.addressesVoted.length === 0 ? ctx.i18n.t('NO') : ctx.i18n.t('YES');
  const link = `${config.DAPP_LINK}/${proposal.type}/${proposal.id}`;

  const contractParts = updateContractTypes.includes(proposal.type)
    ? [
      short ? '' : ctx.i18n.t('PROPOSAL_VOTES_FOR', { votesFor, forPercentage }),
      short ? '' : ctx.i18n.t('PROPOSAL_NOT_VOTED', { votesAgainst, againstPercentage }),
      ctx.i18n.t('PROPOSAL_VOTING_ENDS', { votingEndTime }),
      ctx.i18n.t('PROPOSAL_PROXY', proposal),
      proposal.key === ''
        ? ctx.i18n.t('PROPOSAL_VOTING_IMPLEMENTATION', proposal)
        : ctx.i18n.t('PROPOSAL_VOTING_KEY', proposal),
    ]
    : [
      short ? '' : ctx.i18n.t('PROPOSAL_VOTES_FOR', { votesFor, forPercentage }),
      short ? '' : ctx.i18n.t('PROPOSAL_VOTES_AGAINST', { votesAgainst, againstPercentage }),
      ctx.i18n.t('PROPOSAL_VOTING_ENDS', { votingEndTime }),
      ctx.i18n.t('PROPOSAL_VETO_ENDS', { vetoEndTime }),
      short ? '' : ctx.i18n.t('PROPOSAL_CURRENT_QUORUM', { currentQuorum }),
      ctx.i18n.t('PROPOSAL_REQUIRED_QUORUM', { requiredQuorum }),
      short || addresses.length === 0 ? '' : ctx.i18n.t('PROPOSAL_YOU_VOTED', { vote }),
      ctx.i18n.t('PROPOSAL_REMARK', proposal),
    ];

  const messageParts = [
    ctx.i18n.t('PROPOSAL_TITLE', { title: getProposalTitle(proposal, ctx), id: proposal.id }),
    ctx.i18n.t('PROPOSAL_STATUS', { status: translateProposalStatus(proposal.status, ctx) }),
    ...contractParts,
    ctx.i18n.t('PROPOSAL_VIEW_FULL_DETAILS', { link }),
  ];

  const isUserCandidate = addresses.includes(proposal.candidate);
  if (isUserCandidate && slashingContractTypes.includes(proposal.type)) {
    messageParts.push(ctx.i18n.t('PROPOSAL_SLASHING_CANDIDATE', proposal));
  }

  return messageParts
    .filter(part => part !== '')
    .join('\n');
}
