import { Classification, ProposalStatus } from '@q-dev/q-js-sdk';

import { ZERO_ADDRESS } from '@/constants/adresses';
import { Proposal, ProposalType } from '@/typings/proposals';
import { I18nContext } from '@/typings/telegraf';

export function getProposalTitle (proposal: Proposal, ctx: I18nContext) {
  const classificationMap: Record<Classification, string> = {
    [Classification.BASIC]: ctx.i18n.t('CLASSIFICATION_BASIC'),
    [Classification.DETAILED]: ctx.i18n.t('CLASSIFICATION_DETAILED'),
    [Classification.FUNDAMENTAL]: ctx.i18n.t('CLASSIFICATION_FUNDAMENTAL'),
  };

  switch (proposal.type) {
    case 'constitutionVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_CONSTITUTION', {
        classification: classificationMap[proposal.classification || Classification.BASIC]
      });
    case 'generalUpdateVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_GENERAL_UPDATE');
    case 'emergencyUpdateVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EMERGENCY_UPDATE');
    case 'rootNodesMembershipVoting':
      if (proposal.candidate !== ZERO_ADDRESS && proposal.replaceDest !== ZERO_ADDRESS) {
        return ctx.i18n.t('PROPOSAL_TITLE_ROOT_NODE_SWAPPING');
      }

      return proposal.candidate && proposal.replaceDest === ZERO_ADDRESS
        ? ctx.i18n.t('PROPOSAL_TITLE_ROOT_NODE_ADDING')
        : ctx.i18n.t('PROPOSAL_TITLE_ROOT_NODE_REMOVING');
    case 'eprsMembershipVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPRS_MEMBERSHIP');
    case 'epdrMembershipVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPDR_MEMBERSHIP');
    case 'epqfiMembershipVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPQFI_MEMBERSHIP');
    case 'eprsParametersVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPRS_PARAMETERS');
    case 'epdrParametersVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPDR_PARAMETERS');
    case 'epqfiParametersVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_EPQFI_PARAMETERS');
    case 'rootNodesSlashingVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_ROOT_NODE_SLASHING');
    case 'validatorsSlashingVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_VALIDATOR_SLASHING');
    case 'addressVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_ADDRESS_VOTING');
    case 'upgradeVoting':
      return ctx.i18n.t('PROPOSAL_TITLE_UPGRADE_VOTING');
    default:
      return ctx.i18n.t('PROPOSAL_TITLE_UNKNOWN');
  }
}

export function translateProposalStatus (status: ProposalStatus, ctx: I18nContext) {
  switch (status) {
    case ProposalStatus.PENDING:
      return ctx.i18n.t('PROPOSAL_STATUS_PENDING');
    case ProposalStatus.ACCEPTED:
      return ctx.i18n.t('PROPOSAL_STATUS_ACCEPTED');
    case ProposalStatus.PASSED:
      return ctx.i18n.t('PROPOSAL_STATUS_PASSED');
    default:
      return ctx.i18n.t('PROPOSAL_STATUS_UNKNOWN');
  }
}

export function translateProposalType (type: ProposalType, ctx: I18nContext) {
  switch (type) {
    case 'q':
      return ctx.i18n.t('Q_PROPOSALS');
    case 'rootNode':
      return ctx.i18n.t('ROOT_NODE_PROPOSALS');
    case 'expert':
      return ctx.i18n.t('EXPERT_PROPOSALS');
    case 'slashing':
      return ctx.i18n.t('SLASHING_PROPOSALS');
    case 'contractUpdate':
      return ctx.i18n.t('CONTRACT_UPDATE_PROPOSALS');
    case 'all':
    default:
      return ctx.i18n.t('ALL_PROPOSALS');
  }
}
