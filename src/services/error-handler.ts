import * as Sentry from '@sentry/node';

import { config } from '@/config';

export function initErrorHandler () {
  Sentry.init({
    dsn: config.SENTRY,
    tracesSampleRate: 1.0,
  });

  Sentry.setContext('bot info', {
    websocketURL: config.WEBSOCKET_URL,
    dappURL: config.DAPP_LINK,
  });
}

export function captureError (error: any, ...args: any[]) {
  try {
    Sentry.captureException(error, {
      extra: { details: JSON.stringify(args) }
    });
    console.info(error);
  } catch (error) {
    console.error(error);
  }
}
