import i18next from 'i18next';

import { readUser } from '@/models/User';

import { I18nContext } from '@/typings/telegraf';

import de from '@/locales/de.json';
import en from '@/locales/en.json';
import uk from '@/locales/uk.json';

export async function initI18n () {
  await i18next.init({
    lng: 'en',
    fallbackLng: 'en',
    resources: {
      en: { translation: en },
      de: { translation: de },
      uk: { translation: uk }
    }
  });
}

export async function getI18nContext (id: number) {
  const user = await readUser(id);
  const language = user?.language || 'en';

  return {
    language,
    chat: { id },
    i18n: { t: i18next.getFixedT(language) }
  } as I18nContext;
}
