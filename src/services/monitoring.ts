import express, { Express } from 'express';
import { WebsocketProvider } from 'web3-core';

import { config } from '@/config';

import { captureError } from './error-handler';
import { web3 } from './web3';

let app: Express;

export function initMonitoringApp () {
  app = express();

  app.get('/health', (req, res) => {
    try {
      const isConnected = (web3.currentProvider as WebsocketProvider).connected;
      res.status(isConnected ? 200 : 500)
        .json({ status: isConnected ? 'connected' : 'not_connected' });
    } catch (error) {
      captureError(error);
      res.status(500)
        .json({
          status: 'error',
          message: (error as Error).message,
          stack: (error as Error).stack,
        });
      return;
    }
  });

  const port = config.MONITORING_PORT || 3030;
  app.listen(port, () => {
    console.info(`Monitoring app started on port ${port}`);
  });
}
