import { IntervalBasedCronScheduler, parseCronExpression } from 'cron-schedule';

import { readAllSubscriptions } from '@/models/Subscription';

import { captureError } from '@/services/error-handler';

import { sendMessageToChat } from '@/helpers/messages';
import { getProposalsByType } from '@/helpers/proposals';
import { getProposalsSummary } from '@/helpers/subscriptions';

import { getI18nContext } from './i18n';

// Send summary report every day at 14:00
const cron = parseCronExpression('0 14 * * *');
// Check every 30 minutes
const scheduler = new IntervalBasedCronScheduler(30 * 60 * 1_000);

export async function initScheduler () {
  scheduler.registerTask(cron, async () => {
    try {
      const userSubscriptions = await readAllSubscriptions();
      const combinedProposals = await Promise.all([
        getProposalsByType('q'),
        getProposalsByType('rootNode'),
        getProposalsByType('expert'),
        getProposalsByType('slashing'),
        getProposalsByType('contractUpdate'),
      ]);

      userSubscriptions
        .filter((sub) => sub.subDaily)
        .forEach(async ({ _id }) => {
          const ctx = await getI18nContext(_id);
          sendMessageToChat(_id, getProposalsSummary(ctx, combinedProposals));
        });
    } catch (error) {
      captureError(error);
    }
  });
}
