import { ConstitutionVotingInstance } from '@q-dev/q-js-sdk/lib/contracts/governance/constitution/ConstitutionVotingInstance';

import { ISubscription, readAllSubscriptions } from '@/models/Subscription';

import { proposalContractTypes } from '@/constants/contracts';
import { ProposalContractType } from '@/typings/contracts';

import { captureError } from '@/services/error-handler';
import { getContractInstance } from '@/services/web3';

import { getBackToMenuKeyboard } from '@/helpers/keyboards';
import { createNewProposalMessage, sendMessageToChat } from '@/helpers/messages';
import { getContractProposals } from '@/helpers/proposals';
import { getProposalSubscribeEvent } from '@/helpers/subscriptions';

import { getI18nContext } from './i18n';

export async function initSockets () {
  await Promise.all(proposalContractTypes.map(connectProposalWS));
  console.info('WebSocket listeners initialized');
}

async function connectProposalWS (type: ProposalContractType) {
  try {
    await subscribeOnProposalCreatedEvent(type);
  } catch (error) {
    captureError(error);
    console.error('WebSocket is down:', type);
    console.info('Reconnecting in 30 seconds...', type);
    setTimeout(() => connectProposalWS(type), 30_000);
  }
}

async function subscribeOnProposalCreatedEvent (contractType: ProposalContractType) {
  const contract = await getContractInstance(contractType) as ConstitutionVotingInstance;
  const emitter = contract.instance.events.ProposalCreated();

  emitter.removeAllListeners();
  emitter.on('data', async ({ event }) => {
    if (event !== 'ProposalCreated') return;

    try {
      await sendNewProposals(contractType);
    } catch (error) {
      captureError(error);
    }
  });
}

async function sendNewProposals (type: ProposalContractType) {
  const allSubscriptions = await readAllSubscriptions();
  const activeProposals = await getContractProposals(type);

  const subscribeEvent = getProposalSubscribeEvent(type);
  const subscriptions = allSubscriptions.filter(
    (sub: ISubscription) => sub[subscribeEvent]
  );

  for (const subscription of subscriptions) {
    const ctx = await getI18nContext(subscription._id);
    const message = await createNewProposalMessage(
      ctx,
      activeProposals[activeProposals.length - 1],
    );

    sendMessageToChat(subscription._id, message, {
      reply_markup: { inline_keyboard: getBackToMenuKeyboard(ctx) }
    });
  }
}
