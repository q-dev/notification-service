import mongoose from 'mongoose';
import { Telegraf } from 'telegraf';

import 'module-alias/register';

import {
  handleAccount,
  handleChangeLanguage,
  handleHelp,
  handleLanguage,
  handleMenu,
  handleStart
} from '@/handlers/common';
import {
  handleAllProposals,
  handleExpertProposals,
  handleProposals,
  handleQProposals,
  handleRootProposals,
  handleSlashingProposals,
  handleUpdateProposals
} from '@/handlers/proposals';
import {
  handleDailySubscriptions,
  handleResetSubscribtions,
  handleSubscribe,
  handleSubscribeAll,
  handleSubscribeExpert,
  handleSubscribeQ,
  handleSubscribeRoot,
  handleSubscribeSlashing,
  handleSubscribeUpdate
} from '@/handlers/subscribe';
import {
  handleAddAddress,
  handleClearAccount,
  handleRemoveAddress,
  handleShowAddress
} from '@/handlers/user';

import { config } from '@/config';
import { BotActions } from '@/constants/bot-actions';
import { I18nContext } from '@/typings/telegraf';

import { initErrorHandler } from '@/services/error-handler';
import { initI18n } from '@/services/i18n';
import { initScheduler } from '@/services/scheduler';
import { initContractInstances } from '@/services/web3';
import { initSockets } from '@/services/ws';

import { useChat } from '@/middlewares/use-chat';
import { useI18n } from '@/middlewares/use-i18n';

import { initMonitoringApp } from './services/monitoring';

export const bot = new Telegraf<I18nContext>(config.TELEGRAM_TOKEN);

async function init () {
  try {
    initErrorHandler();
    initContractInstances();

    await mongoose.connect(config.DATABASE_URL);
    await initI18n();

    initSockets();
    initScheduler();
    initMonitoringApp();

    bot.use(useChat);
    bot.use(useI18n);

    initBotHandlers();

    bot.launch();
    console.info('Bot started');
  } catch (error) {
    console.error('Failed to initialize bot', error);
  }
}

function initBotHandlers () {
  bot.command(BotActions.START, handleStart);
  bot.command(BotActions.HELP, handleHelp);
  bot.command(BotActions.ADD, handleAddAddress);

  bot.action(BotActions.PROPOSALS, handleProposals);
  bot.action(BotActions.Q_PROPOSALS, handleQProposals);
  bot.action(BotActions.EXPERT_PROPOSALS, handleExpertProposals);
  bot.action(BotActions.ROOT_PROPOSALS, handleRootProposals);
  bot.action(BotActions.SLASHING_PROPOSALS, handleSlashingProposals);
  bot.action(BotActions.UPDATES_PROPOSALS, handleUpdateProposals);
  bot.action(BotActions.ALL_PROPOSALS, handleAllProposals);

  bot.action(BotActions.SUBSCRIPTIONS, handleSubscribe);
  bot.action(BotActions.SUBSCRIBE_Q, handleSubscribeQ);
  bot.action(BotActions.SUBSCRIBE_EXPERT, handleSubscribeExpert);
  bot.action(BotActions.SUBSCRIBE_ROOT, handleSubscribeRoot);
  bot.action(BotActions.SUBSCRIBE_SLASHING, handleSubscribeSlashing);
  bot.action(BotActions.SUBSCRIBE_UPDATE, handleSubscribeUpdate);
  bot.action(BotActions.SUBSCRIBE_ALL, handleSubscribeAll);
  bot.action(BotActions.DAILY_SUBSCRIPTIONS, handleDailySubscriptions);
  bot.action(BotActions.RESET_SUBSCRIPTIONS, handleResetSubscribtions);

  bot.action(BotActions.MENU, handleMenu);
  bot.action(BotActions.HELP, handleHelp);
  bot.action(BotActions.ACCOUNT, handleAccount);

  bot.action(BotActions.LANGUAGE, handleLanguage);
  bot.action(BotActions.ENGLISH, ctx => handleChangeLanguage(ctx, 'en'));
  // TODO: Enable when German translations added
  // bot.action(BotActions.GERMAN, ctx => handleChangeLanguage(ctx, 'de'));
  bot.action(BotActions.UKRAINIAN, ctx => handleChangeLanguage(ctx, 'uk'));

  bot.action(BotActions.ADDRESSES, handleShowAddress);
  bot.action(BotActions.CLEAR_ACCOUNT, handleClearAccount);
  bot.on(BotActions.CALLBACK_QUERY, handleRemoveAddress);
}

init();
