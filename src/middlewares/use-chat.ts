import { Context } from 'telegraf';

import { readUser } from '@/models/User';

import { ChatContext } from '@/typings/telegraf';

import { captureError } from '@/services/error-handler';

export async function useChat (ctx: Context, next: () => Promise<any>) {
  if (!ctx.chat?.id) return;

  try {
    const user = await readUser(ctx.chat.id);
    (ctx as ChatContext).language = user?.language || 'en';
  } catch (e) {
    captureError(e);
    return;
  }

  return next();
}
