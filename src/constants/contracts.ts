import { ContractType, ProposalContractType } from '@/typings/contracts';

export const qContractTypes: ProposalContractType[] = [
  'constitutionVoting',
  'emergencyUpdateVoting',
  'generalUpdateVoting',
];

export const rootNodeContractTypes: ProposalContractType[] = [
  'rootNodesMembershipVoting',
];

export const expertMembershipContractTypes: ProposalContractType[] = [
  'epqfiMembershipVoting',
  'epdrMembershipVoting',
  'eprsMembershipVoting',
];

export const expertParametersContractTypes: ProposalContractType[] = [
  'epqfiParametersVoting',
  'epdrParametersVoting',
  'eprsParametersVoting',
];

export const expertContractTypes: ProposalContractType[] = [
  ...expertMembershipContractTypes,
  ...expertParametersContractTypes,
];

export const slashingContractTypes: ProposalContractType[] = [
  'rootNodesSlashingVoting',
  'validatorsSlashingVoting',
];

export const updateContractTypes: ProposalContractType[] = [
  'upgradeVoting',
  'addressVoting',
];

export const proposalContractTypes: ProposalContractType[] = [
  ...qContractTypes,
  ...rootNodeContractTypes,
  ...slashingContractTypes,
  ...expertContractTypes,
  ...updateContractTypes
];

export const supportedContractTypes: ContractType[] = [
  ...proposalContractTypes,
  'rootNodes'
];

export const noVetoContractTypes: ContractType[] = [
  'addressVoting',
  'upgradeVoting',
  'validatorsSlashingVoting',
  'emergencyUpdateVoting',
];
