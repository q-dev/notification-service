export enum BotActions {
  START = 'start',
  HELP = 'help',
  ADD = 'add',
  MENU = 'menu',
  ACCOUNT = 'account',

  PROPOSALS = 'proposals',
  Q_PROPOSALS = 'q_proposals',
  EXPERT_PROPOSALS = 'expert_proposals',
  ROOT_PROPOSALS = 'root_proposals',
  SLASHING_PROPOSALS = 'slashing_proposals',
  UPDATES_PROPOSALS = 'updates_proposals',
  ALL_PROPOSALS = 'all_proposals',

  SUBSCRIPTIONS = 'subscriptions',
  SUBSCRIBE_Q = 'subscribe_q',
  SUBSCRIBE_EXPERT = 'subscribe_expert',
  SUBSCRIBE_ROOT = 'subscribe_root',
  SUBSCRIBE_SLASHING = 'subscribe_slashing',
  SUBSCRIBE_UPDATE = 'subscribe_update',
  SUBSCRIBE_ALL = 'subscribe_all',
  DAILY_SUBSCRIPTIONS = 'daily_subscriptions',
  RESET_SUBSCRIPTIONS = 'reset_subscriptions',

  CLEAR_ACCOUNT = 'clear_account',
  ADDRESSES = 'addresses',

  LANGUAGE = 'language',
  ENGLISH = 'english',
  GERMAN = 'german',
  UKRAINIAN = 'ukrainian',

  TEXT = 'text',
  CALLBACK_QUERY = 'callback_query',
}
