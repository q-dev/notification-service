# Q Governance Notifications Telegram Bot

Notification Bot service for the Q Development AG.

## Technologies used
- Node.js 
- TypeScript
- Telegraf
- Web3
- MongoDB

## Project structure

- `/src` – Main source folder
  - `/constants` – Constants & enums
  - `/handlers` – Bot commands & actions
  - `/helpers` – Contract & message helper functions
  - `/locales` – I18n translations
  - `/middlewares` – Command interceptors & guards
  - `/models` – MongoDB data models
  - `/services` – Web3, I18n, WebSocket service providers
  - `/typings` – TypeScript typings
  - `/utils` – Formatters & util functions

## Environment

To run the bot you need to create `.env` file inside the project directory.

`.env` file structure:
```bash
# System contract registry address.
# More details: https://docs.qtestnet.org/dapp-dashboard/#blockchain
CONTRACT_REGISTRY_ADDRESS=0xc3E589056Ece16BCB88c6f9318e9a7343b663522
# Telegram Bot Token.
# Generate one using Bot Father Telegram Bot: https://t.me/BotFather
TELEGRAM_TOKEN=<BOT_FATHER_TOKEN>
# Sentry error tracking service link.
# More details: https://sentry.io
SENTRY=<SENTRY_URL>
# MongoDB connection string
DATABASE_LINK=mongodb://127.0.0.1:27017/Notification-Service?directConnection=true&serverSelectionTimeoutMS=2000
# WebSocket RPC endpoint URL
WEBSOCKET_LINK=wss://rpc-ws.q.org
# Base proposal URL in Your HQ DApp
DAPP_LINK=https://hq.q.org/governance/proposal
# Port for monitoring endpoint, 3030 by default
MONITORING_PORT=3030
```

## Local development

### Install dependencies
```bash
yarn install
```

### Run in development mode
```bash
yarn dev
```

### Build for production
```bash
yarn build
```

### Build & Run in production mode 
```bash
yarn start
```

## Docker

### Start local service

```bash
docker-compose up
```

### Start local mongo instance

```bash
docker-compose -f mongo.yml up
```

### Clear local mongo data

```bash
docker-compose -f mongo.yml down -v
```
